# Profiler

> `Data Infuser` / Profiler 프로젝트 입니다.

Data Infuser 하위 프로젝트.

Loader를 통해 적재한 데이터에 대한 프로파일링 작업 요청을 위해 DB Queue로 구현한 Job Queue에 요청 작업을 삽입하면,  
스케줄러를 통해 주기적으로 적재된 요청 작업에 따라 프로파일링을 수행하는 모듈입니다.

## Environment

- Spring boot v2.3.5
- jdk 11.0.10
- MariaDB v10.5.6
- MySQL v8.0.21
- DataCleaner v5.7.1 lib (exclude : DataCleaner lib 내 slf4j, gson, elasticSearch 관련 lib 제거; - Spring boot dependency와 충돌)

## Installation

- DataCleaner 5.7.1 다운로드

  > https://github.com/datacleaner/DataCleaner/releases/download/DataCleaner-5.7.1/DataCleaner-5.7.1.zip

  > 다운로드 후 lib 디렉토리 내 파일들 -> Project root path에 libs 디렉토리 생성 후 이동

- build.gradle -> dependencies 추가 (build.gradle 참고)

  > compile group: 'org.mariadb.jdbc', name: 'mariadb-java-client', version: '2.6.0'

  > compile group: 'mysql', name: 'mysql-connector-java', version: '8.0.21'

  > compile fileTree(dir: 'libs', include: ['*.jar'])

  > compile group: 'org.springframework.boot', name: 'spring-boot-configuration-processor'

  > compile group: 'org.flywaydb', name: 'flyway-core'

  > providedRuntime('org.springframework.boot:spring-boot-starter-tomcat')

- lombok 설치 (Use IntelliJ IDEA)

  > lombok plugin install

  > build.gradle -> compileOnly 'org.projectlombok:lombok' 추가

## Usage (배포 전)

- Project > resources > application.yml 작성. (application-sample.yml 참고)

## How to run(local)

- intellij

  > - cd src/main/resources/ </br>
  > - application-sample.yml application.yml

  > - profiler database 내 job_queue 테이블에 프로파일링 수행할 테이블 등록
  > - (target_title에 테이블 명 기입)

  > - project run.

  > - /GET : localhost:8080/profile/:table_name 으로 프로파일링 결과 조회

- Use gradle

  > ./gradlew bootRun -Dspring.profiles.active=ENV

- Use executable jar

  > ./gradlew bootJar && cd build/lib

  > java -noverify -Dspring.profiles.active=ENV -jar filename.jar

  > nohup java -noverify -Dspring.profiles.active=ENV -jar filename.jar > /dev/null 2>&1 & (background 실행 시)

## BUILD and RUN For Production env

- Build
  > ./gradlew bootJar
- Use Dockerfile

  > docker build --build-arg SPRING_PROFILES_ACTIVE=<ENV> -t profiler .

  > docker run --rm --name <CONTAINER_NAME> --env-file env.sh -t profiler

- -- 참고사항 --

  > NCP SourceBuild 사용 시 gradle 6.x 버전을 지원하지 않음.
  > 따라서, .gradle 디렉토리도 포함하여 업로드 후 빌드 권장. (.gitignore에서 .gradle 해제)

- Run Example

```sh
docker build --build-arg SPRING_PROFILES_ACTIVE=prod -t profiler .
docker run -p 8080:8080 --rm -d --name profiler --env-file ./env.sh -t profiler
```

### Environment variables

- ENV list (application.yml > profiles 정의)
  > local | test | prod

| 환경변수               | 설명                                   |
| ---------------------- | -------------------------------------- |
| SPRING_PROFILES_ACTIVE | 구동 환경 레벨 ( local / test / prod ) |
| DB_USER                | DB 접속 사용자 명                      |
| DB_PW                  | DB 접속 사용자 비밀번호                |
| DB_URL_PROFILER        | 프로파일러 관련 DB URL                 |
| DB_URL_META            | 메타 관련 DB URL                       |
| DB_URL_DATASET         | 적재 데이터셋 DB URL                   |

## Meta

Promptechnology - [@Homepage](http://www.promptech.co.kr/) - [dev@promptech.co.kr](dev@promptech.co.kr)

프로젝트는 LGPL 3.0 라이센스로 개발되었습니다. 자세한 사항은 http://www.gnu.org/licenses/lgpl-3.0.txt 를 확인해주세요.

Licensed under the Lesser General Public License, See http://www.gnu.org/licenses/lgpl-3.0.txt for more information.

## Support

![alt text](http://wisepaip.org/assets/home/promptech-d8574a0910561aaea077bc759b1cf94c07baecc551f034ee9c7e830572d671de.png "Title Text")
