package kr.co.promptech.profiler.controller;

import kr.co.promptech.profiler.model.dto.ProfileDto;
import kr.co.promptech.profiler.model.profile.History;
import kr.co.promptech.profiler.service.profile.HistoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/profile")
public class ApiController {

    private final HistoryService historyService;

    @GetMapping("/{name}")
    public ProfileDto findAll(@PathVariable("name") String tableName) {
        return historyService.findWithResultVfDTO(tableName);
    }
}
