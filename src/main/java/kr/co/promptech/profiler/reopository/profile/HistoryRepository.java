package kr.co.promptech.profiler.reopository.profile;

import kr.co.promptech.profiler.model.profile.History;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * profile_target에 대한 JpaRepository 클래스
 */
@Repository
public interface HistoryRepository extends JpaRepository<History, Integer> {

    History findByTargetTitle(String title);

    boolean existsByTargetTitle(String title);

    @Query(value = "SELECT DISTINCT h FROM History h " +
            "LEFT JOIN FETCH h.results r " +
            "WHERE h.targetTitle = :tableName")
    History findWithResultVF(@Param("tableName") String tableName);
}
