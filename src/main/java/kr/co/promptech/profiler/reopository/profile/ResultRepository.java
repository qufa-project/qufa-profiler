package kr.co.promptech.profiler.reopository.profile;

import kr.co.promptech.profiler.model.profile.History;
import kr.co.promptech.profiler.model.profile.Result;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface ResultRepository extends JpaRepository<Result, Integer> {

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM result WHERE result.history_id = :id", nativeQuery = true)
    void deleteAllByQuery(@Param("id") int id);

    void deleteAllByHistory(History history);
}
