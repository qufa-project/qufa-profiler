package kr.co.promptech.profiler.service.profile;

import kr.co.promptech.profiler.config.DataSourceHolder;
import kr.co.promptech.profiler.model.dto.ProfileDto;
import kr.co.promptech.profiler.model.profile.History;
import kr.co.promptech.profiler.model.profile.Result;
import kr.co.promptech.profiler.model.status.HistoryStatus;
import kr.co.promptech.profiler.reopository.profile.HistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * profile_target에 대한 서비스를 수행하는 클래스
 */
@RequiredArgsConstructor
@Service
@Transactional
public class HistoryService {

    private final HistoryRepository historyRepository;

    private final ResultService resultService;

    public History findOrCreate(String title) {
        DataSourceHolder.setDataSourceType("profiler");

        History history = new History();

        if (historyRepository.existsByTargetTitle(title)) {
            history = historyRepository.findByTargetTitle(title);

            List<Result> results = history.getResults();
            results.forEach(el -> {
                el.getResultVFList().clear();
            });
            results.clear();

            resultService.deleteAllByHistory(history);
        }

        history.setTargetTitle(title);
        history.setStatus(HistoryStatus.PENDING);

        this.save(history);

        return history;

    }

    public void addResult(Result result, String title) {
        DataSourceHolder.setDataSourceType("profiler");

        History history = historyRepository.findByTargetTitle(title);

        history.getResults().add(result);
        result.setHistory(history);
    }

    public void delete(History history) {
        historyRepository.delete(history);
    }

    public void save(History history) {
        historyRepository.save(history);
    }

    public History findWithResultVF(String tableName) {
        DataSourceHolder.setDataSourceType("profiler");

        return historyRepository.findWithResultVF(tableName);
    }

    public ProfileDto findWithResultVfDTO(String tableName) {
        DataSourceHolder.setDataSourceType("profiler");

        History history = this.findWithResultVF(tableName);
        return ProfileDto.objectToDTO(history);
    }

    public History findByTableName(String tableName) {
        DataSourceHolder.setDataSourceType("profiler");

        History history = historyRepository.findByTargetTitle(tableName);

        DataSourceHolder.clear();

        return history;
    }
}
