package kr.co.promptech.profiler.service;

import kr.co.promptech.profiler.config.ActiveProfileProperty;
import kr.co.promptech.profiler.config.DataSourceHolder;
import kr.co.promptech.profiler.model.profile.Result;
import kr.co.promptech.profiler.model.profile.ResultVF;
import kr.co.promptech.profiler.utils.ColumnTypeChecker;
import kr.co.promptech.profiler.utils.logger.GlobalLoggerFactory;
import kr.co.promptech.profiler.utils.logger.LogType;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.datacleaner.api.AnalyzerResult;
import org.datacleaner.api.InputColumn;
import org.datacleaner.beans.*;
import org.datacleaner.beans.valuedist.ValueDistributionAnalyzer;
import org.datacleaner.beans.valuedist.ValueDistributionAnalyzerResult;
import org.datacleaner.job.AnalysisJob;
import org.datacleaner.job.builder.AnalysisJobBuilder;
import org.datacleaner.job.builder.AnalyzerComponentBuilder;
import org.datacleaner.job.runner.AnalysisResultFuture;
import org.datacleaner.job.runner.AnalysisRunner;
import org.datacleaner.job.runner.AnalysisRunnerImpl;
import org.datacleaner.result.ValueFrequency;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 데이터 프로파일 작업을 수행하는 서비스
 */
@RequiredArgsConstructor
@Component
@Transactional
public class ProfileService {
    /**
     *
     */
    private final ActiveProfileProperty activeProfileProperty;

    private List<Result> resultList;

    private GlobalLoggerFactory globalLoggerFactory = GlobalLoggerFactory.getInstance(this.getClass());

    /**
     * 타겟 테이블의 모든 컬럼에 대해 프로파일을 수행하는 메소드
     *
     * @param tableName   타겟 테이블명
     * @param columnNames 타겟 테이블의 컬럼리스트
     */
    public List<Result> profileColumns(String tableName, List<String> columnNames) {
        DataSourceHolder.setDataSourceType("dataset");

        DataStoreService.createDataStore(activeProfileProperty.getActive());

        resultList = new ArrayList<>();

        for (String columnName : columnNames) {

            this.profileSingleColumn(tableName, columnName);
        }

        DataSourceHolder.clear();
        return resultList;
    }

    /**
     * 타겟 테이블의 한 컬럼별 프로파일을 수행하는 메소드
     *
     * @param tableName  타겟 테이블명
     * @param columnName 타겟 테이블의 1개 컬럼
     */
    private void profileSingleColumn(String tableName, String columnName) {

        String inputColumnName = String.format("%s.%s.%s", DataStoreService.getSchemaName(), tableName, columnName);
        globalLoggerFactory.simpleLog("inputColumnName : " + inputColumnName, LogType.INFO);

        AnalysisJobBuilder builder = DataStoreService.getBuilder();
        builder.addSourceColumns(inputColumnName);
        InputColumn<?> targetInputColumn = builder.getSourceColumnByName(columnName);
        String columnTypeClass = targetInputColumn.getDataType().getTypeName();
        String columnType = targetInputColumn.getPhysicalColumn().getType().getName();

        // analyzer config
        AnalyzerComponentBuilder<ValueDistributionAnalyzer> valDistAnalyzer = builder.addAnalyzer(ValueDistributionAnalyzer.class);
        valDistAnalyzer.addInputColumns(targetInputColumn);
        valDistAnalyzer.setConfiguredProperty(ValueDistributionAnalyzer.PROPERTY_RECORD_UNIQUE_VALUES, true);
        valDistAnalyzer.setConfiguredProperty(ValueDistributionAnalyzer.PROPERTY_RECORD_DRILL_DOWN_INFORMATION, true);

        if (ColumnTypeChecker.isString(columnTypeClass)) {
            AnalyzerComponentBuilder<StringAnalyzer> stringAnalyzer = builder.addAnalyzer(StringAnalyzer.class);
            stringAnalyzer.addInputColumn(targetInputColumn);

            // stringAnalyzer.setConfiguredProperty("Descriptive statistics", false);
        } else if (ColumnTypeChecker.isNumber(columnTypeClass)) {
            AnalyzerComponentBuilder<NumberAnalyzer> numberAnalyzer = builder.addAnalyzer(NumberAnalyzer.class);
            numberAnalyzer.setConfiguredProperty("Descriptive statistics", true);
            numberAnalyzer.addInputColumn(targetInputColumn);
        }
//        else if (ColumnTypeChecker.isDate(columnTypeClass)) {
//            AnalyzerComponentBuilder<DateAndTimeAnalyzer> numberAnalyzer = builder.addAnalyzer(DateAndTimeAnalyzer.class);
//            numberAnalyzer.addInputColumn(targetInputColumn);
//      }
        else if (ColumnTypeChecker.isBoolean(columnTypeClass)) {
            AnalyzerComponentBuilder<BooleanAnalyzer> booleanAnalyzer = builder.addAnalyzer(BooleanAnalyzer.class);
            booleanAnalyzer.addInputColumn(targetInputColumn);
        }

        // job build run & result
        AnalysisJob analysisJob = builder.toAnalysisJob();
        AnalysisRunner runner = new AnalysisRunnerImpl(DataStoreService.getConfiguration());
        AnalysisResultFuture resultFuture = runner.run(analysisJob);

        resultFuture.await();
        globalLoggerFactory.simpleLog("==========run===========", LogType.INFO);
        // 에러 발생 혹은 취소시,
        if (resultFuture.isCancelled() || resultFuture.isErrornous()) {
            globalLoggerFactory.simpleLog("profiling error !", LogType.ERROR);
            List<Throwable> errors = resultFuture.getErrors();
            globalLoggerFactory.simpleLog(errors.toString(), LogType.ERROR);

            resultFuture.cancel();
        } else {
            // 성공시 결과 저장.
            List<AnalyzerResult> results = resultFuture.getResults();
            globalLoggerFactory.simpleLog(" ============  RESULTS ============= ", LogType.INFO);
            globalLoggerFactory.simpleLog("\n" + results.toString(), LogType.INFO);
            globalLoggerFactory.simpleLog("====== extract Results ========", LogType.INFO);
            this.extractResult(results, targetInputColumn, columnName, columnType);

            // 빌더 리셋 후 재시작
            builder.reset();
        }
    }

    /**
     * 프로파일 결과를 추출하여 DB에 저장하는 메소드
     *
     * @param results           프로파일 결과에 대한 Resultset
     * @param targetInputColumn 현재 프로파일 작업을 수행한 컬럼
     * @param columnName        현재 프로파일 작업을 수행한 컬럼명
     */
    private void extractResult(List<AnalyzerResult> results,
                               InputColumn<?> targetInputColumn,
                               String columnName,
                               String columnType) {
        Result resultModel = new Result();
        int totalCnt = 0;

        List<ResultVF> vfModelList = new ArrayList<>();

        resultModel.setColumnType(columnType);
        resultModel.setNullCnt(0);
        resultModel.setBlankCnt(0);

        resultModel.setColumnName(columnName);
        //TODO: COLUMN COMMENT 가져오기
//        resultModel.setColumnDesc(columnInfo.getColumnDesc());

        for (AnalyzerResult result : results) {
            if (result instanceof ValueDistributionAnalyzerResult) {
                if (((ValueDistributionAnalyzerResult) result).getNullCount() > 0) {
                    resultModel.setNullCnt(((ValueDistributionAnalyzerResult) result).getNullCount());
                }

                resultModel.setDistinctCnt(((ValueDistributionAnalyzerResult) result).getDistinctCount());
                resultModel.setRowCnt(((ValueDistributionAnalyzerResult) result).getTotalCount());
                resultModel.setUniqueCnt(((ValueDistributionAnalyzerResult) result).getUniqueCount());

                Collection<ValueFrequency> vfList = ((ValueDistributionAnalyzerResult) result).getValueCounts();
                for (ValueFrequency vf : vfList) {
                    if (vf.getChildren() != null) {
                        Collection<ValueFrequency> vfChildren = vf.getChildren();
                        for (ValueFrequency vfChild : vfChildren) {
                            ResultVF profileValueModel = new ResultVF();

                            profileValueModel.setColumnName(columnName);
//                            TODO: COLUMN COMMENT 가져오기
//                            resultVFModel.setColumnDesc(columnInfo.getColumnDesc());
                            profileValueModel.setColumnGroupVal(vfChild.getValue());
                            profileValueModel.setColumnGroupCount(vfChild.getCount());
                            vfModelList.add(profileValueModel);
                        }
                    } else {
                        ResultVF profileValueModel = new ResultVF();

                        profileValueModel.setColumnName(columnName);
                        //TODO: COLUMN COMMENT 가져오기
//                        resultVFModel.setColumnDesc(columnInfo.getColumnDesc());
                        profileValueModel.setColumnGroupVal(vf.getValue());
                        profileValueModel.setColumnGroupCount(vf.getCount());
                        vfModelList.add(profileValueModel);
                    }
                }

                totalCnt = ((ValueDistributionAnalyzerResult) result).getTotalCount();
            }
        }

        for (AnalyzerResult result : results) {
            if (result instanceof StringAnalyzerResult) {
                if (((StringAnalyzerResult) result).getNullCount(targetInputColumn) > 0 && resultModel.getNullCnt() == 0) {
                    resultModel.setNullCnt(((StringAnalyzerResult) result).getNullCount(targetInputColumn));
                }

                if (((StringAnalyzerResult) result).getNullCount(targetInputColumn) < totalCnt) {
                    resultModel.setStrAvgLenVal(((StringAnalyzerResult) result).getAvgChars(targetInputColumn));
                    resultModel.setStrMaxLenVal(((StringAnalyzerResult) result).getMaxChars(targetInputColumn));
                    resultModel.setStrMinLenVal(((StringAnalyzerResult) result).getMinChars(targetInputColumn));
                }
                resultModel.setBlankCnt(((StringAnalyzerResult) result).getBlankCount(targetInputColumn));
            }

//            if (result instanceof StringAnalyzerResult) {
//                if (totalCnt > 0 && resultModel.getNullCnt() != 0) {
//                    resultModel.setStrAvgLenVal(((StringAnalyzerResult) result).getAvgChars(targetInputColumn));
//                    resultModel.setStrMaxLenVal(((StringAnalyzerResult) result).getMaxChars(targetInputColumn));
//                    resultModel.setStrMinLenVal(((StringAnalyzerResult) result).getMinChars(targetInputColumn));
//                    resultModel.setBlankCnt(((StringAnalyzerResult) result).getBlankCount(targetInputColumn));
//                }
//            }

            if (result instanceof NumberAnalyzerResult) {
                if (((NumberAnalyzerResult) result).getHighestValue(targetInputColumn) != null) {
                    resultModel.setNumMaxVal((Double) ((NumberAnalyzerResult) result).getHighestValue(targetInputColumn));
                }
                if (((NumberAnalyzerResult) result).getLowestValue(targetInputColumn) != null) {
                    resultModel.setNumMinVal((Double) ((NumberAnalyzerResult) result).getLowestValue(targetInputColumn));
                }
                if (((NumberAnalyzerResult) result).getMean(targetInputColumn) != null) {
                    resultModel.setNumMeanVal((Double) ((NumberAnalyzerResult) result).getMean(targetInputColumn));
                }
                if (((NumberAnalyzerResult) result).getMedian(targetInputColumn) != null) {
                    resultModel.setNumMedianVal((Double) ((NumberAnalyzerResult) result).getMedian(targetInputColumn));
                }
            }

            if (result instanceof BooleanAnalyzerResult) {
                if (((BooleanAnalyzerResult) result).getTrueCount() != null) {
                    resultModel.setTrueCnt((Integer) ((BooleanAnalyzerResult) result).getTrueCount().getValue(columnName));
                }
                if (((BooleanAnalyzerResult) result).getFalseCount() != null) {
                    resultModel.setFalseCnt((Integer) ((BooleanAnalyzerResult) result).getFalseCount().getValue(columnName));
                }
            }

//            if (result instanceof DateAndTimeAnalyzerResult) {
//                if (((DateAndTimeAnalyzerResult) result).getNullCount(targetInputColumn) > 0 && resultModel.getNullCnt() == 0) {
//                    resultModel.setNullCnt(((DateAndTimeAnalyzerResult) result).getNullCount(targetInputColumn));
//                    // TODO: Result 엔티티에 DateTime 관련 컬럼 추가
//                }
//            }
//

        }

        resultModel.setDuplicateCnt(totalCnt - resultModel.getDistinctCnt());
        resultModel.setFrquentMaxVal(findFrequentMaxVal(vfModelList));
        resultModel.setFrquentMinVal(findFrequentMinVal(vfModelList));

        resultModel.setResultVFList(vfModelList);
        resultList.add(resultModel);

    }

    /**
     * Value Frequency 분석값 중 최다 빈도값을 찾는 메소드
     *
     * @param profileValues Value Frequency 분석에 대한 Resultset
     * @return 최다 빈도수 데이터 반환
     */
    private String findFrequentMaxVal(List<ResultVF> profileValues) {
        int max = 0;
        int index = -1;

        if (profileValues.size() == 0 || (profileValues.size() == 1 && StringUtils.isBlank(profileValues.get(0).getColumnGroupVal()))) {
            return null;
        }

        for (int i = 0; i < profileValues.size(); i++) {
            String val = profileValues.get(i).getColumnGroupVal();
            if (StringUtils.isBlank(val)) {
                continue;
            }

            if (max < profileValues.get(i).getColumnGroupCount()) {
                max = profileValues.get(i).getColumnGroupCount();
                index = i;
            }
        }

//        if (index < 0) {
//            return null;
//        }
        return this.limitStrLength(profileValues.get(index).getColumnGroupVal());
    }

    /**
     * Value Frequency 분석값 중 최소 빈도값을 찾는 메소드
     *
     * @param profileValues Value Frequency 분석에 대한 Resultset
     * @return 최다 빈도수 데이터 반환
     */
    private String findFrequentMinVal(List<ResultVF> profileValues) {
        int min = Integer.MAX_VALUE;
        int index = -1;

        if (profileValues.size() == 0 || (profileValues.size() == 1 && StringUtils.isBlank(profileValues.get(0).getColumnGroupVal()))) {
            return null;
        }

        for (int i = 0; i < profileValues.size(); i++) {
            String val = profileValues.get(i).getColumnGroupVal();
            if (StringUtils.isBlank(val)) {
                continue;
            }

            if (min > profileValues.get(i).getColumnGroupCount()) {
                min = profileValues.get(i).getColumnGroupCount();
                index = i;
            }
        }

//        if (index < 0) {
//            return null;
//        }
        return this.limitStrLength(profileValues.get(index).getColumnGroupVal());
    }

    private String limitStrLength(String str) {

        if (str.length() > 255) {

            return str.substring(0, 250) + "....";
        }

        return str;
    }

//    private boolean isRemovalColumn(String tableName, String columnName, Map<String, Set<String>> removals) {
//        if (!removals.containsKey(tableName)) {
//            return false;
//        }
//
//        Set<String> removalColumns = removals.get(tableName);
//        return removalColumns.contains(columnName);
//    }
}

