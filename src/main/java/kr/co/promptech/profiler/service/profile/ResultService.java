package kr.co.promptech.profiler.service.profile;

import kr.co.promptech.profiler.config.DataSourceHolder;
import kr.co.promptech.profiler.model.profile.History;
import kr.co.promptech.profiler.reopository.profile.ResultRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Service
@Transactional
public class ResultService {

    private final ResultRepository resultRepository;

    public void deleteAll(int targetId) {
        DataSourceHolder.setDataSourceType("profiler");

        resultRepository.deleteAllByQuery(targetId);

        DataSourceHolder.clear();
    }

    public void deleteAllByHistory(History history) {
        DataSourceHolder.setDataSourceType("profiler");

        resultRepository.deleteAllByHistory(history);

        DataSourceHolder.clear();
    }
}
