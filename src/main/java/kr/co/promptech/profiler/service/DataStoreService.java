package kr.co.promptech.profiler.service;

import kr.co.promptech.profiler.utils.logger.GlobalLoggerFactory;
import kr.co.promptech.profiler.utils.logger.LogType;
import org.datacleaner.configuration.DataCleanerConfiguration;
import org.datacleaner.configuration.DataCleanerConfigurationImpl;
import org.datacleaner.configuration.JaxbConfigurationReader;
import org.datacleaner.connection.Datastore;
import org.datacleaner.connection.DatastoreCatalogImpl;
import org.datacleaner.connection.JdbcDatastore;
import org.datacleaner.job.builder.AnalysisJobBuilder;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * 프로파일링을 위한 기본 설정 작업을 수행하는 클래스 <br>
 * Configruation 파일 파싱, Datasource 연결, 프로파일 분석 작업 설정 등을 수행한다
 */
@Component
public class DataStoreService {

    public static String driver;
    public static String url;
    public static String username;
    public static String password;

    public static final String DEFAULT_DB = "local";
    public static final String PROD_DB = "prod";
    public static final String TEST_DB = "test";

    public static final String CONFIG_FILE_PATH = "config/datastore_config.xml";

    private static DataCleanerConfiguration configuration;
    private static AnalysisJobBuilder builder;
    private static Datastore dataStore;

    private GlobalLoggerFactory globalLoggerFactory = GlobalLoggerFactory.getInstance(this.getClass());

    /**
     * Configuration파일을 파싱하여 DB를 연결하는 메소드
     */
    public static void createDataStore() {

        InputStream dbInputStream;

        try {
            dbInputStream = new ClassPathResource(CONFIG_FILE_PATH).getInputStream();

            JaxbConfigurationReader dbConfigurationReader = new JaxbConfigurationReader();

            configuration = dbConfigurationReader.read(dbInputStream);

            setDefault(DEFAULT_DB);
        } catch (FileNotFoundException e) {
            GlobalLoggerFactory.getInstance(DataStoreService.class).logException(e, LogType.ERROR);
        } catch (IOException e) {
            GlobalLoggerFactory.getInstance(DataStoreService.class).logException(e, LogType.ERROR);
        } finally {
            dbInputStream = null;
            System.gc();
        }
    }

    public static void createDataStore(String dbName) {

        try {
            JdbcDatastore datastore = new JdbcDatastore(dbName, url, driver, username, password, false);

            DatastoreCatalogImpl catalog = new DatastoreCatalogImpl(datastore);

            configuration = new DataCleanerConfigurationImpl(null, null, catalog, null);

            setDefault(dbName);
        } finally {
            System.gc();
        }
    }

    public static Datastore getDataStore() {
        return dataStore;
    }

    public static String getSchemaName() {
        String[] _url = url.split("\\?")[0].split("/");
        return _url[_url.length-1];
    }

    public static AnalysisJobBuilder setDataStore(String dbName) {
        dataStore = configuration.getDatastoreCatalog().getDatastore(dbName);

        GlobalLoggerFactory.getInstance(DataStoreService.class).simpleLog("dataStore name: " + dataStore.toString(), LogType.INFO);

        return builder.setDatastore(dataStore);
    }

    public static DataCleanerConfiguration getConfiguration() {
        return configuration;
    }

    public static AnalysisJobBuilder getBuilder() {
        return builder;
    }

    public static void setDefault(String DBName) {
        try {
            builder = new AnalysisJobBuilder(configuration);
            if (configuration != null) {
                setDataStore(DBName);
            }
        } catch (Exception e) {
            GlobalLoggerFactory.getInstance(DataStoreService.class).logException(e, LogType.ERROR);
        }

        System.gc();
    }

    public static void setDriver(String driver) {
        DataStoreService.driver = driver;
    }

    public static void setUrl(String url) {
        DataStoreService.url = url;
    }

    public static void setUsername(String username) {
        DataStoreService.username = username;
    }

    public static void setPassword(String password) {
        DataStoreService.password = password;
    }
}
