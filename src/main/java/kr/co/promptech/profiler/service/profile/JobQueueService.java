package kr.co.promptech.profiler.service.profile;

import kr.co.promptech.profiler.config.DataSourceHolder;
import kr.co.promptech.profiler.model.profile.JobQueue;
import kr.co.promptech.profiler.reopository.profile.JobQueueRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * profile_queue(Job_queue)에 대한 서비스를 담당하는 클래스
 */
@Service
@RequiredArgsConstructor
public class JobQueueService {

    private final JobQueueRepository jobQueueRepository;

    public List<JobQueue> findAll() {
        DataSourceHolder.setDataSourceType("profiler");
        
        return jobQueueRepository.findAllByOrderByIdAsc();
    }

    public void save(JobQueue jobQueue) {
        DataSourceHolder.setDataSourceType("profiler");

        jobQueueRepository.save(jobQueue);
    }

    public void delete(JobQueue jobQueue) {
        DataSourceHolder.setDataSourceType("profiler");

        jobQueueRepository.delete(jobQueue);
    }
}
