package kr.co.promptech.profiler.service.meta;

import kr.co.promptech.profiler.config.DataSourceHolder;
import kr.co.promptech.profiler.reopository.meta.MetaColumnRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * meta_column에 대한 서비스를 담당하는 클래스
 */
@RequiredArgsConstructor
@Service
@Transactional(readOnly = true)
public class MetaColumnService {

    private final MetaColumnRepository metaColumnRepository;

    public List<String> findAllColumnsByServiceId(int serviceId) {
        DataSourceHolder.setDataSourceType("meta");

        List<String> columns = metaColumnRepository.findAllColumnsByServiceId(serviceId);

        DataSourceHolder.clear();
        return columns;
    }

    public List<String> findAllColumns(String tableName, String schemaName) {
        DataSourceHolder.setDataSourceType("meta");

        List<String> columns = metaColumnRepository.findAllColumns(tableName, schemaName);

        DataSourceHolder.clear();
        return columns;
    }
}
