package kr.co.promptech.profiler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Spring Boot 프로젝트 메인 클래스
 */
@SpringBootApplication(
        exclude = {
                DataSourceAutoConfiguration.class
        }
)
@EnableTransactionManagement
@EnableScheduling
@EnableAsync
//@Import({DataSourceConfig.class, DataSourceProperty.class})
public class ProfilerApplication extends SpringBootServletInitializer {
    /**
     * 메소드 테스트용
     *
     * @param args 파람 테스트
     */
    public static void main(String[] args) {

        SpringApplication.run(ProfilerApplication.class, args);

    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ProfilerApplication.class);
    }
}
