package kr.co.promptech.profiler.utils.logger;

public enum LogType {
    INFO, ERROR, DEBUG, WARN
}
