package kr.co.promptech.profiler.utils;

import java.util.Arrays;

public class ColumnTypeChecker {
    private static final String[] TYPE_STRING = {"java.lang.String"};
    private static final String[] TYPE_NUMBER = {
            "java.lang.Number", "java.lang.Integer", "java.lang.Long", "java.lang.Short",
            "java.lang.Double", "java.lang.Float", "java.math.BigInteger", "java.math.BigDecimal"
    };

    private static final String[] TYPE_DATE = {"java.util.Date", "java.time.Instant"};
    private static final String[] TYPE_BOOLEAN = {"java.lang.Boolean"};

    public static boolean isString(String typeName) {
        return Arrays.asList(TYPE_STRING).contains(typeName);
    }

    public static boolean isNumber(String typeName) {
        return Arrays.asList(TYPE_NUMBER).contains(typeName);
    }

    public static boolean isDate(String typeName) {
        return Arrays.asList(TYPE_DATE).contains(typeName);
    }

    public static boolean isBoolean(String typeName) {
        return Arrays.asList(TYPE_BOOLEAN).contains(typeName);
    }
}
