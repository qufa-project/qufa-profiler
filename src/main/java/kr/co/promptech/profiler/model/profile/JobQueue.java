package kr.co.promptech.profiler.model.profile;

import kr.co.promptech.profiler.model.status.JobQueueStatus;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * profile_queue 테이블과 매핑되는 엔티티 클래스
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "job_queue")
public class JobQueue {
    /**
     * 고유 ID (PK)
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * 프로파일링 대상 테이블명
     */
    @Column(name = "target_title")
    private String targetTitle;

    /**
     * 프로파일링 작업 상태 : <br>
     *     enum('wait', 'doing', 'failed')
     */
    @Enumerated(EnumType.STRING)
    private JobQueueStatus status;

    /**
     * failed 작업 재시도 횟수
     */
    @Column(columnDefinition = "int default 0", name = "retry_cnt")
    private int retryCnt;

    /**
     * 생성일시
     */
    @Column(name = "created_at")
    @CreationTimestamp
    private LocalDateTime createdAt;

    /**
     * 수정일시
     */
    @Column(name = "updated_at")
    @UpdateTimestamp
    private LocalDateTime updatedAt;


    @Builder
    public JobQueue(String targetTitle, JobQueueStatus status) {
        this.targetTitle = targetTitle;
        this.status = status;
    }
}
