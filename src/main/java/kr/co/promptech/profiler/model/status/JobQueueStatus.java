package kr.co.promptech.profiler.model.status;

public enum JobQueueStatus {
    ASSIGNED,
    FAILED
}
