package kr.co.promptech.profiler.model.profile;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class ProfileValue {

    /**
     * 컬럼명
     */
    private String columnName;

    /**
     * 컬럼 코멘트 정보
     */
    private String columnDesc;

    /**
     * 그룹별 레코드값
     */
    private String columnGroupVal;

    /**
     * 그룹별 레코드 개수
     */
    private Integer columnGroupCount;
}
