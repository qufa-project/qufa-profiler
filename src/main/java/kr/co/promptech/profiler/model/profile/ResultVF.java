package kr.co.promptech.profiler.model.profile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "result_vf")
public class ResultVF {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private String id;

    @Column(name = "column_name")
    @JsonIgnore
    private String columnName;

    @Column(name = "column_desc")
    @JsonIgnore
    private String columnDesc;

    @Column(name = "column_group_val")
    private String columnGroupVal;

    @Column(name = "column_group_cnt")
    private Integer columnGroupCount;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "result_id")
    private Result result;
}
