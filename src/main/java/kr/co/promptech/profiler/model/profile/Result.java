package kr.co.promptech.profiler.model.profile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * profile_detail 테이블과 매핑되는 엔티티 클래스
 */
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "result")
public class Result {
    public static final int PERMIT_MIN_VF_COUNT = 10;

    /**
     * 고유 ID (PK)
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private int id;

    /**
     * 컬럼명
     */
    @Column(name = "column_name")
    private String columnName;

    /**
     * 컬럼 코멘트 정보
     */
    @Column(name = "column_desc")
    private String columnDesc;

    /**
     * 컬럼 타입 정보
     */
    @Column(name = "column_type")
    private String columnType;

    /**
     * row 개수
     */
    @Column(name = "row_cnt")
    private Integer rowCnt;

    /**
     * NULL 개수
     */
    @Column(name = "null_cnt")
    private Integer nullCnt;

    /**
     * unique 값 개수
     */
    @Column(name = "unique_cnt")
    private Integer uniqueCnt;

    /**
     * 중복을 제외한 레코드 개수
     */
    @Column(name = "distinct_cnt")
    private Integer distinctCnt;
    /**
     * 중복 레코드 개수
     */
    @Column(name = "duplicate_cnt")
    private Integer duplicateCnt;

    /**
     * 공백 레코드 개수
     */
    @Column(name = "blank_cnt")
    private Integer blankCnt;

    /**
     * 참 레코드 개수
     */
    @Column(name = "true_cnt")
    private Integer trueCnt;

    /**
     * 거짓 레코드 개수
     */
    @Column(name = "false_cnt")
    private Integer falseCnt;

    /**
     * 문자열 레코드 최소 길이
     */
    @Column(name = "str_min_len_val")
    private Integer strMinLenVal;

    /**
     * 문자열 레코드 최대 길이
     */
    @Column(name = "str_max_len_val")
    private Integer strMaxLenVal;

    /**
     * 문자열 레코드 평균 길이
     */
    @Column(name = "str_avg_len_val")
    private Double strAvgLenVal;

    /**
     * 숫자형 레코드 최소값
     */
    @Column(name = "num_min_val")
    private Double numMinVal;

    /**
     * 숫자형 레코드 최대값
     */
    @Column(name = "num_max_val")
    private Double numMaxVal;

    /**
     * 숫자형 레코드 중앙값
     */
    @Column(name = "num_median_val")
    private Double numMedianVal;

    /**
     * 숫자형 레코드 평균값
     */
    @Column(name = "num_mean_val")
    private Double numMeanVal;

    /**
     * 최소 빈도 레코드 값
     */
    @Column(name = "frquent_min_val")
    private String frquentMinVal;

    /**
     * 최다 빈도 레코드 값
     */
    @Column(name = "frquent_max_val")
    private String frquentMaxVal;

    /**
     * profile_target_id (FK)
     */
    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "history_id")
    private History history;

    /**
     * profile_detail 테이블과 일대다 양방향 매핑
     */
    @OneToMany(mappedBy = "result", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<ResultVF> resultVFList = new ArrayList<>();

    public void setResultVFList(List<ResultVF> resultVFList) {
        resultVFList.forEach(el-> {
            el.setResult(this);
        });
        this.resultVFList = resultVFList;
    }
}
