package kr.co.promptech.profiler.model.profile;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;
import kr.co.promptech.profiler.model.status.HistoryStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * profile_target 테이블과 매핑되는 엔티티 클래스
 */
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "history")
public class History {
    /**
     * 고유 ID (PK)
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private int id;

    /**
     * 프로파일링 대상 테이블명
     */
    @Column(name = "target_title")
    @JsonAlias("dataset_name")
    private String targetTitle;

    @Enumerated(EnumType.STRING)
    @JsonIgnore
    private HistoryStatus status;

    @Column(name = "error_msg", columnDefinition = "text")
    @JsonIgnore
    private String errorMsg;

    /**
     * 생성일시
     */
    @Column(name="created_at")
    @CreationTimestamp
    @JsonIgnore
    private LocalDateTime createdAt;

    /**
     * 수정일시
     */
    @Column(name="updated_at")
    @UpdateTimestamp
    @JsonIgnore
    private LocalDateTime updatedAt;

    /**
     * profile_detail 테이블과 일대다 양방향 매핑
     */
    @OneToMany(mappedBy = "history", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Result> results = new ArrayList<>();
}
