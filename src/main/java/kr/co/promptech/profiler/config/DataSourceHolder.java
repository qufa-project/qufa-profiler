package kr.co.promptech.profiler.config;

public class DataSourceHolder {
    private static final ThreadLocal<String> ctxHolder = new ThreadLocal<>();

    public static String getDataSourceType() {
        return ctxHolder.get();
    }

    public static void setDataSourceType(String name) {
        ctxHolder.set(name);
    }

    public static void clear() {
        ctxHolder.remove();
    }
}
