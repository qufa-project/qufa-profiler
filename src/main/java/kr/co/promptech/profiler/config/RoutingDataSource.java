package kr.co.promptech.profiler.config;

import kr.co.promptech.profiler.utils.logger.GlobalLoggerFactory;
import kr.co.promptech.profiler.utils.logger.LogType;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import java.util.Map;

public class RoutingDataSource extends AbstractRoutingDataSource {

    private GlobalLoggerFactory globalLoggerFactory = GlobalLoggerFactory.getInstance(this.getClass());

    @Override
    public void setTargetDataSources(Map<Object, Object> targetDataSources) {
        super.setTargetDataSources(targetDataSources);

    }

    @Override
    protected Object determineCurrentLookupKey() {
        globalLoggerFactory.simpleLog("current datasource using : " + DataSourceHolder.getDataSourceType(), LogType.INFO);

        return DataSourceHolder.getDataSourceType();
    }
}