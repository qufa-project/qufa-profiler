package kr.co.promptech.profiler.config;

import kr.co.promptech.profiler.service.DataStoreService;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.List;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties("datasources")
@Getter
@Setter
public class DataSourceProperty {

    private String username;
    private String password;
    private String driverClassName;
    private List<Datasource> datasourceList;

    @Getter
    @Setter
    public static class Datasource {
        private String name;
        private String url;
    }

    @PostConstruct
    public void init() {
        DataStoreService.setDriver(driverClassName);
        DataStoreService.setUsername(username);
        DataStoreService.setPassword(password);

        datasourceList.forEach(el -> {
            if(el.getName().equals("dataset")) {
                DataStoreService.setUrl(el.getUrl());
            }
        });
    }
}
