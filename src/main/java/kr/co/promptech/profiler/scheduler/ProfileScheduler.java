package kr.co.promptech.profiler.scheduler;

import kr.co.promptech.profiler.model.profile.History;
import kr.co.promptech.profiler.model.profile.JobQueue;
import kr.co.promptech.profiler.model.profile.Result;
import kr.co.promptech.profiler.model.status.HistoryStatus;
import kr.co.promptech.profiler.model.status.JobQueueStatus;
import kr.co.promptech.profiler.service.DataStoreService;
import kr.co.promptech.profiler.service.ProfileService;
import kr.co.promptech.profiler.service.meta.MetaColumnService;
import kr.co.promptech.profiler.service.profile.HistoryService;
import kr.co.promptech.profiler.service.profile.JobQueueService;
import kr.co.promptech.profiler.utils.logger.GlobalLoggerFactory;
import kr.co.promptech.profiler.utils.logger.LogType;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 비동기 방식의 스케줄러 구현 클래스로, profile_queue에 등록된 작업을 통해 프로파일링을 수행한다.
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class ProfileScheduler {

    private final JobQueueService jobQueueService;
    private final HistoryService historyService;
    private final MetaColumnService metaColumnService;
    private final ProfileService profileService;

    private History history;

    private GlobalLoggerFactory globalLoggerFactory = GlobalLoggerFactory.getInstance(this.getClass());

    /**
     * 비동기 방식의 스케줄러 작동 메소드 <br>
     * 일정 시간마다 profile_queue를 탐색하여 프로파일링을 수행한다.
     */
    // 60000 ms = 1분, 작업 완료시간으로부터 타이머
    @Scheduled(fixedDelay = 30000)
    public void runProfiling() {
        globalLoggerFactory.simpleLog("\n----------------------------------Start profile scheduler----------------------------------\n", LogType.INFO);
        //job_queue의 target_title은 schema.table 형식으로 삽입되어야함.
        List<JobQueue> jobQueueList = jobQueueService.findAll();

        for (JobQueue jobQueue : jobQueueList) {
            try {
                jobQueue.setStatus(JobQueueStatus.ASSIGNED);
                String targetTitle = jobQueue.getTargetTitle();
                globalLoggerFactory.simpleLog("Target: " + targetTitle, LogType.INFO);
                history = historyService.findOrCreate(targetTitle);

//                int targetId = Integer.parseInt(targetTitle.split("-")[1]);
                // 인퓨저 DB 내 service id를 통해 테이블의 컬럼 리스트를 가져옴
//                List<String> targetCols = metaColumnService.findAllColumnsByServiceId(targetId);

                // 데이터셋이 저장된 schema명을 가져옴
                String schemaName = DataStoreService.getSchemaName();
                globalLoggerFactory.simpleLog("SchemaName: " + schemaName, LogType.INFO);
                log.debug("schemaName : " + schemaName);
                // MySQL information.schema를 통해 컬럼 리스트를 가져옴
                List<String> targetCols = metaColumnService.findAllColumns(targetTitle, schemaName);
                globalLoggerFactory.simpleLog(targetCols.toString(), LogType.INFO);

                List<Result> resultSet = profileService.profileColumns(targetTitle, targetCols);
                resultSet.forEach(rs -> {
                    rs.setHistory(history);
                });

                // profile success
                jobQueueService.delete(jobQueue);

                history.setResults(resultSet);
                history.setStatus(HistoryStatus.SUCCESS);

                historyService.save(history);

                globalLoggerFactory.simpleLog("\n----------------------------------Done profiling by one table----------------------------------\n", LogType.INFO);
            } catch (Exception e) {
                globalLoggerFactory.logException(e, LogType.ERROR);

                // job_queue에 타겟 테이블 status = failed로 update
                jobQueue.setStatus(JobQueueStatus.FAILED);
                jobQueue.setRetryCnt(jobQueue.getRetryCnt() + 1);

                // retry 횟수 1번 초과시 job_queue에서 삭제.
                if (jobQueue.getRetryCnt() > 1) {
                    jobQueueService.delete(jobQueue);
                } else {
                    jobQueueService.save(jobQueue);
                }

                StringBuilder sb = new StringBuilder();
                sb.append(e.getMessage());
                for (StackTraceElement el : e.getStackTrace()) {
                    sb.append("\n\t at " + el);
                }
                String errMsg = sb.toString();

                history.setErrorMsg(errMsg);
                history.setStatus(HistoryStatus.FAILED);
                historyService.save(history);

                System.gc();

                globalLoggerFactory.simpleLog("\n----------------------------------Profiling failed----------------------------------\n", LogType.INFO);
            }
        }
        globalLoggerFactory.simpleLog("----------------------------------Done sheduler----------------------------------", LogType.INFO);
    }
}
