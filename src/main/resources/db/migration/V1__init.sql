create table profile_queue
(
    id           int auto_increment
        primary key,
    retryCnt    int          not null,
    status       varchar(255) null,
    targetTitle varchar(255) null,
    createdAt   datetime     null,
    updatedAt   datetime     null
);

create table profile_error
(
    id               int auto_increment
        primary key,
    errorMsg        longtext null,
    createdAt       datetime null,
    updatedAt       datetime null,
    profileQueueId int      null,
    constraint FKb7maif5sgy17r6x5akvtp5o77
        foreign key (profileQueueId) references profile_queue (id)
);

create table profile_target
(
    id    int auto_increment
        primary key,
    title varchar(255) null,
    createdAt datetime null,
    updatedAt datetime null
);

create table profile_detail
(
    id                int auto_increment
        primary key,
    blankCnt         int          null,
    columnDesc       varchar(255) null,
    columnName       varchar(255) null,
    distinctCnt      int          null,
    duplicateCnt     int          null,
    frquentMaxVal   varchar(255) null,
    frquentMinVal   varchar(255) null,
    nullCnt          int          null,
    numMaxVal       double       null,
    numMeanVal      double       null,
    numMedianVal    double       null,
    numMinVal       double       null,
    rowCnt           int          null,
    strAvgLenVal   double       null,
    strMaxLenVal   int          null,
    strMinLenVal   int          null,
    profileTargetId int          null,
    constraint FK1gifkxmqnp4c8ryul58dyboi6
        foreign key (profileTargetId) references profile_target (id)
);

create table profile_value
(
    id                 int auto_increment
        primary key,
    columnDesc        varchar(255) null,
    columnGroupCount int          null,
    columnGroupVal   varchar(255) null,
    columnName        varchar(255) null,
    profileTargetId  int          null,
    constraint FKpyttsl49jwk7y3urdgk06vxje
        foreign key (profileTargetId) references profile_target (id)
);