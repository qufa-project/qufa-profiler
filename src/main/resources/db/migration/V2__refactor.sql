drop table if exists profile_error;
drop table if exists profile_queue;
drop table if exists profile_value;
drop table if exists profile_detail;
drop table if exists profile_target;

create table if not exists job_queue
(
    id           int auto_increment
        primary key,
    retry_cnt    int          not null DEFAULT 0,
    status       varchar(255) null,
    target_title varchar(255) null,
    created_at   datetime     null,
    updated_at   datetime     null
);

create table if not exists history
(
    id    int auto_increment
        primary key,
    target_title varchar(255) null,
    status varchar(255) null,
    error_msg longtext null,
    created_at datetime null,
    updated_at datetime null
);

create table if not exists result
(
    id                int auto_increment
        primary key,
    blank_cnt         int          null,
    column_desc       varchar(255) null,
    column_name       varchar(255) null,
    row_cnt           int          null,
    null_cnt          int          null,
    distinct_cnt      int          null,
    duplicate_cnt     int          null,
    frquent_max_val   varchar(255) null,
    frquent_min_val   varchar(255) null,
    num_max_val       double       null,
    num_mean_val      double       null,
    num_median_val    double       null,
    num_min_val       double       null,
    str_avg_len_val   double       null,
    str_max_len_val   int          null,
    str_min_len_val   int          null,
    history_id int          null,
    foreign key (history_id) references history (id)
);