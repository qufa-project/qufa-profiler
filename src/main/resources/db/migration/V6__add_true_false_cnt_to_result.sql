ALTER TABLE `result`
    ADD COLUMN true_cnt int(11) AFTER str_min_len_val,
    ADD COLUMN false_cnt int(11) AFTER true_cnt;